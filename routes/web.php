<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ChamadoController;
use App\Http\Controllers\Admin\FluxoCaixaController;
use App\Http\Controllers\Admin\PlataformaController;
use App\Http\Controllers\Admin\ClienteController;
use App\Http\Controllers\Admin\ProdutoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::any('/', [IndexController::class, 'index']);


# Logout
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {

        Route::any('/clonar', [IndexController::class, 'clonar'])->name('clonar');
        Route::any('/limpar', [IndexController::class, 'limpar'])->name('limpar');

        Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'dashboard'])->name('admin.dashboard');
        
        Route::get('/user/lista', [App\Http\Controllers\Admin\UserController::class, 'user'])->name('admin.user.lista');
   	    
        Route::group(['prefix' => 'cliente'], function() {
            Route::get('/info/{id}', [ClienteController::class, 'info'])->name('admin.cliente.info');
            Route::any('/lista', [ClienteController::class, 'lista'])->name('admin.cliente.lista');
            Route::any('/add', [ClienteController::class, 'add'])->name('admin.cliente.add');
            Route::any('/editar/{id}', [ClienteController::class, 'editar'])->name('admin.cliente.editar');
            Route::any('/delete', [ClienteController::class, 'delete'])->name('admin.cliente.delete');
        });

        Route::group(['prefix' => 'produto'], function() {
            Route::get('/info/{id}', [ProdutoController::class, 'info'])->name('admin.produto.info');
            Route::any('/lista', [ProdutoController::class, 'lista'])->name('admin.produto.lista');
            Route::any('/add', [ProdutoController::class, 'add'])->name('admin.produto.add');
            Route::any('/editar/{id}', [ProdutoController::class, 'editar'])->name('admin.produto.editar');
            Route::any('/delete', [ProdutoController::class, 'delete'])->name('admin.produto.delete');
        });

        # Controle Financeiro | Fluxo de Caixa
        Route::any('/controle-financeiro/lista/fluxo-caixa', [FluxoCaixaController::class, 'lista'])->name('admin.controle-financeiro.lista.fluxo-caixa');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-entrada', [FluxoCaixaController::class, 'addEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-saida', [FluxoCaixaController::class, 'addSaida'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-saida');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_vencimento', [FluxoCaixaController::class, 'editarDataVencimento'])->name('admin.controle-financeiro.lista.editar-datavencimento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_lancamento', [FluxoCaixaController::class, 'editarDataLancamento'])->name('admin.controle-financeiro.lista.editar-datalancamento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_recebimento', [FluxoCaixaController::class, 'editarDataRecebimento'])->name('admin.controle-financeiro.lista.editar-datarecebimento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-data_estorno', [FluxoCaixaController::class, 'editarDataEstorno'])->name('admin.controle-financeiro.lista.editar-dataestorno');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-obs', [FluxoCaixaController::class, 'editarObs'])->name('admin.controle-financeiro.lista.editar-obs');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-motivo_estorno', [FluxoCaixaController::class, 'editarMotivoEstorno'])->name('admin.controle-financeiro.lista.editar-motivoestorno');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-valor', [FluxoCaixaController::class, 'editarValor'])->name('admin.controle-financeiro.lista.editar-valor');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-referencia', [FluxoCaixaController::class, 'editarReferencia'])->name('admin.controle-financeiro.lista.editar-referencia');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-link', [FluxoCaixaController::class, 'editarLink'])->name('admin.controle-financeiro.lista.editar-link');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-formapagamento', [FluxoCaixaController::class, 'editarFormaPagamento'])->name('admin.controle-financeiro.lista.editar-formapagamento');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-vendedor', [FluxoCaixaController::class, 'editarVendedor'])->name('admin.controle-financeiro.lista.editar-vendedor');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-unidade', [FluxoCaixaController::class, 'editarUnidade'])->name('admin.controle-financeiro.lista.editar-unidade');
        Route::any('/controle-financeiro/lista/fluxo-caixa/add-entrada', [FluxoCaixaController::class, 'addEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.add-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/deletar-entrada', [FluxoCaixaController::class, 'deletarEntrada'])->name('admin.controle-financeiro.lista.fluxo-caixa.deletar-entrada');
        Route::any('/controle-financeiro/lista/fluxo-caixa/deletar-saida', [FluxoCaixaController::class, 'deletarSaida'])->name('admin.controle-financeiro.lista.fluxo-caixa.deletar-saida');
        Route::any('/controle-financeiro/lista/fluxo-caixa/conversar/{aluno_id}', [FluxoCaixaController::class, 'conversar'])->name('admin.controle-financeiro.lista.fluxo-caixa.conversar');
        Route::any('/controle-financeiro/lista/fluxo-caixa/getDadosEntradaById', [FluxoCaixaController::class, 'getDadosEntradaById'])->name('admin.controle-financeiro.lista.fluxo-caixa.getDadosEntradaById');
        Route::any('/controle-financeiro/lista/fluxo-caixa/getDadosSaidaById', [FluxoCaixaController::class, 'getDadosSaidaById'])->name('admin.controle-financeiro.lista.fluxo-caixa.getDadosSaidaById');
        Route::any('/controle-financeiro/lista/fluxo-caixa/editar-setstatus', [FluxoCaixaController::class, 'setStatus'])->name('admin.controle-financeiro.lista.editar-setstatus');
        

        # Chamado
        Route::get('/chamado/lista', [ChamadoController::class, 'lista'])->name('admin.chamado.lista');
        Route::get('/chamado/info/{id}', [ChamadoController::class, 'info'])->name('admin.chamado.info');
       
        Route::any('/plataforma/info/{id}', [App\Http\Controllers\Admin\PlataformaController::class, 'info'])->name('admin.plataforma.info');
       
    });
});