<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questao extends Model
{
    use HasFactory;

    protected $table = 'questoes';

    public $timestamps = true;

    public $fillable = [
        'plataforma_id',
        'cadastrante_id',
        'assunto',
        'enunciado',
        'arquivo',
        'modelo', // (D)iscursiva ou (O)bjetiva
        'gabarito',
        'status',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
