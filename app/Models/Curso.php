<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $table = 'cursos';

	public $timestamps = true;

    public $fillable = [

        'plataforma_id',
        'cadastrante_id',
        'certificado_id',

        'nome', // nome comercial
        'referencia', // nome interno para ADM
        'slug',

        'carga_horaria', // ex: 1.500 horas
        'tipo_formacao', // ex: Bacharelado
        'nivel',         // ex: Graduação

        'descricao',
        'foto_capa',
        'video',

        'status',
        'publicar',

        // SEO
        'seo_title',
        'seo_keywords',
        'seo_description',

    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

}
