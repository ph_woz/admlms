<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrilhaTurma extends Model
{
    use HasFactory;

    protected $table = 'trilhas_turmas';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'trilha_id',
        'turma_id',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }
}
