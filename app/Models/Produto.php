<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
    protected $table = 'adm_produtos';

    public $timestamps = true;

    public $fillable = [
        'cadastrante_id',
        'nome', 
        'mensalista',
        'status',
        'descricao', 
        'created_at',
    ];

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
