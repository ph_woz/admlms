<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FluxoCaixaCategoria extends Model
{
    use HasFactory;

    protected $table = 'fluxo_caixa_categorias';

    public $timestamps = false;

    protected $fillable = ['plataforma_id','categoria_id', 'fluxo_caixa_id'];
}
