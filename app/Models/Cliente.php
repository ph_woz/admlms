<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'adm_clientes';

    public $timestamps = true;

    public $fillable = [
        'cadastrante_id',
        'nome', 
        'email', 
        'status',
        'rg',
        'cpf',
        'cnpj',
        'site',
        'genero',
        'data_nascimento',
        'celular',
        'telefone',
        'obs',
        'cep',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'complemento',
        'ponto_referencia',
        'pais',
        'created_at',
    ];

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
