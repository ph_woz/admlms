<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlataformaModulo extends Model
{
    use HasFactory;

    protected $table = 'plataformas_modulos';

    public $timestamps = false;

    protected $fillable = [
    	'plataforma_id',
    	'texto',
    	'url',
    	'status'
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {   
        return $query->where('status', 0);
    }

    public static function meusModulosSidebar()
    {
        $modulos = \DB::table('plataformas_modulos')
            ->join('acl_plataformas_modulos_users', 'plataformas_modulos.id', 'acl_plataformas_modulos_users.modulo_id')
            ->where('plataformas_modulos.status', 0)
            ->where('plataformas_modulos.plataforma_id', session('plataforma_id'))
            ->where('acl_plataformas_modulos_users.user_id', \Auth::id())
            ->where('acl_plataformas_modulos_users.nivel', '>', 0)
            ->select('plataformas_modulos.id','plataformas_modulos.url','plataformas_modulos.texto')
            ->get();

        return $modulos;
    }
    
}
