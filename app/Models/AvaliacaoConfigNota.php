<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvaliacaoConfigNota extends Model
{
    use HasFactory;

    protected $table = 'avaliacoes_config_notas';

    public $timestamps = false;

    public $fillable = [
    	'plataforma_id',
    	'avaliacao_id',
    	'n_questoes',
        'valor_nota',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }
}
