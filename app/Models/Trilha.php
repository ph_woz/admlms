<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trilha extends Model
{
    use HasFactory;

    protected $table = 'trilhas';

    public $timestamps = true;

    protected $fillable = [
        'plataforma_id',
        'formulario_id',
        'certificado_id',
        'nome',
        'referencia',
        'slug',
        'tipo_inscricao',
        'foto_capa',
        'publicar',
        'carga_horaria',
        'tipo_formacao',
        'duracao',
        'nivel',
        'video',
        'descricao',
        'valor_parcelado',
        'valor_a_vista',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
    ];

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }
    
    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }
}
