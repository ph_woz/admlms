<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    use HasFactory;

    protected $table = 'turmas';

	public $timestamps = true;

    public $fillable = [
    	
        'plataforma_id',
        'curso_id',
        'formulario_id',
        'cadastrante_id',

        'nome',
        'status',

        'tipo_inscricao',
        'publicar',
        'modalidade',
        'data_inicio',
        'data_termino',
        'frequencia',
        'horario',
        'duracao',

        'valor_parcelado',
        'valor_a_vista',

        'nota_minima_aprovacao',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopePublico($query)
    {
        return $query->where('publicar', 'S');
    }

}
