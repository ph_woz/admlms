<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formulario extends Model
{
    use HasFactory;

    protected $table = 'formularios';

    public $timestamps = true;

    public $fillable = [
    	'plataforma_id',
        'cadastrante_id',
    	'unidade_id',
        'referencia',
        'slug',
    	'status',
        'texto',
        'texto_retorno',
        'texto_botao_retorno',
    	'link_retorno',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

}
