<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoAula extends Model
{
    use HasFactory;
    protected $table = 'cursos_aulas';

    public $timestamps = false;

    public $fillable = [

    	'plataforma_id',
    	'cadastrante_id',
    	'curso_id',
    	'modulo_id',
    	'nome',
        'descricao',
        'tipo_objeto',
        'conteudo',
    	'ordem',
    	'status',
    ];

    public function getPlataforma()
    {
        return $this->belongsTo(Plataforma::class, 'plataforma_id');
    }

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }
    
    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }
}
