<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'adm_users';

    protected $fillable = [
        'cadastrante_id',
        'nome',
        'email',
        'password',
        'cargo',
        'celular',
        'data_nascimento',
        'data_ultimo_acesso',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function scopeAluno($query)
    {
        return $query->where('tipo', 'aluno');
    }

    public function scopeColaborador($query)
    {
        return $query->where('tipo', 'colaborador');
    }

}
