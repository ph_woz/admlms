<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Util extends Model
{

    public static function replaceDateEn($data) {

        if(is_null($data))
            return null;

        $data = str_replace("/", "-", $data);
        $data = date("Y-m-d", strtotime($data));

        return $data;
    }

    public static function replaceDatePt($data) {

        if(is_null($data)) 
            return null;

        $data = date('d/m/Y', strtotime($data) );

        return $data;
    }

    public static function replaceDateTimePt($data) {

        if(is_null($data)) 
            return null;

        $data = date('d/m/Y H:i', strtotime($data) );

        return $data;
    }

    public static function replaceDateTimeEn($data) {

        if(is_null($data))
            return null;

        $data = str_replace("/", "-", $data);
        $data = date("Y-m-d H:i", strtotime($data));
        
        return $data;
    }

    public static function getIdade($data_nascimento) {

		$idade = date('Y', strtotime($data_nascimento));
		$idade = date('Y') - $idade;

    	return $idade;
    } 

    public static function generateHash()
    {
        $hash = substr(str_shuffle("123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);
        return $hash;
    }

    public static function replacePrimeiroNome($nome)
    {
        $nomeExplode = explode(' ', $nome);
        
        if(isset($nomeExplode[1]))
            return $nomeExplode[0];
        else
            return $nome;
    }

    public static function getPrimeiroSegundoNome($nome)
    {
        $names = explode(' ', $nome);
        $nome = (isset($names[1])) ? $names[0]. ' ' .$names[1] : $names[0];

        if (array_key_exists(2, $names))
            $names2 = $names[2];
        else
            $names2 = null;

        if(\Str::of(strtolower($nome))->endsWith('de') || \Str::of(strtolower($nome))->endsWith('dos') || \Str::of(strtolower($nome))->endsWith('da'))
            $nome = $nome . ' ' . $names2 ?? null;

        return $nome;
    }

    public static function getMeses()
    {
        $meses = collect([
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro',
        ]);

        return $meses;
    }

    public static function getLinkStorage()
    {
        return 'https://wozcodelms2.s3.amazonaws.com/';
    }

    public static function getValorRealInArray($valoresFaturamento)
    {
        $convertValoresToFloat = [];

        foreach($valoresFaturamento as $valor)
        {
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);

            $convertValoresToFloat[] = floatval($valor);
        }

        $faturamento = number_format(array_sum($convertValoresToFloat) , 2,",",".");

        return $faturamento;
    }

    public static function getUserNomeById($id)
    {
        $nome = User::where('id', $id)->pluck('nome')[0] ?? null;

        return $nome;
    }

    public static function getPlataformaNomeById($id)
    {
        $nome = Plataforma::where('id', $id)->pluck('nome')[0] ?? null;

        return $nome;
    }

    public static function proximosMesesParaRepetirRegistro()
    {
        $meses = collect();

        foreach(Util::getMeses() as $numero => $nome)
        {
            if($numero > date('m'))
                $meses->push(['numero' => $numero, 'nome' => $nome]);  
        }

        return $meses;
    }

}
