<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    use HasFactory;

    protected $table = 'certificados';

	public $timestamps = true;

    public $fillable = [
        
        'plataforma_id',
        'cadastrante_id',

        'referencia',

        'modelo',
        'posicao_imagem',

        'nome_posicao_x',
        'nome_posicao_y',
        'nome_fontSize',

        'texto1',
        'texto1_posicao_x',
        'texto1_posicao_y',
        'texto1_fontSize',

        'texto2',
        'texto2_posicao_x',
        'texto2_posicao_y',
        'texto2_fontSize',

        'codigo_posicao_x',
        'codigo_posicao_y',
        'codigo_fontSize',

        'status',
    ];

    public function scopePlataforma($query)
    {
        return $query->where('plataforma_id', session('plataforma_id') ?? Plataforma::dominio()->pluck('id')[0]);
    }

    public function scopeAtivo($query)
    {
        return $query->where('status', 0);
    }

    public function replaceVariaveis($text, $user_id, $turma_id)
    {
        $estado = null;
        $data_nascimento = null;
        $data_conclusao = null;

        if($user_id)
        {
            $user = User::plataforma()->find($user_id);

            $estadoAluno = UserEndereco::plataforma()->where('user_id', $user->id)->pluck('estado')[0] ?? null;

            $data_nascimento = Util::replaceDatePt($user->data_nascimento) ?? null;
        }

        if($turma_id)
        {
            $curso_id  = Turma::plataforma()->where('id', $turma_id)->pluck('curso_id')[0] ?? null;
            $curso = Curso::plataforma()->where('id', $curso_id)->first();

            $data_conclusao = TurmaAluno::plataforma()->where('turma_id', $turma_id)->where('aluno_id', $user_id)->pluck('data_conclusao_curso')[0] ?? null;
            $data_conclusao = Util::replaceDatePt($data_conclusao);
        }

        $text = str_replace('[NOME_ALUNO]', $user->nome ?? '[NOME_ALUNO]', $text);
        $text = str_replace('[CURSO]', $curso->nome ?? null, $text);
        $text = str_replace('[DATA_NASCIMENTO]', $data_nascimento ?? '[DATA_NASCIMENTO]', $text);
        $text = str_replace('[CPF]', $user->cpf ?? '[CPF]', $text);
        $text = str_replace('[CARGA_HORARIA]', $curso->carga_horaria ?? '[CARGA_HORARIA]', $text);
        $text = str_replace('[DATA_CONCLUSAO]', $data_conclusao ?? request('data_conclusao') ?? '[DATA_CONCLUSAO]', $text);
        $text = str_replace('[DATA_ATUAL]', date('d/m/Y'), $text);
        $text = str_replace('[ESTADO_ALUNO]', $estadoAluno ?? '[ESTADO_ALUNO]', $text);
        $text = str_replace('\n', PHP_EOL, $text);

        return $text;
    }

}
