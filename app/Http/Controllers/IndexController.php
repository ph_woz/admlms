<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\PlataformaModulo;
use App\Models\AclPlataformaModuloUser;
use App\Models\UserLMS;
use App\Models\Certificado;
use App\Models\PaginaConteudo;
use App\Models\Formulario;
use App\Models\FormularioPergunta;
use App\Models\Turma;
use App\Models\Curso;
use App\Models\TrilhaTurma;
use App\Models\Trilha;
use App\Models\CursoModulo;
use App\Models\CursoAula;
use App\Models\Avaliacao;
use App\Models\AvaliacaoConfigNota;
use App\Models\AvaliacaoQuestao;
use App\Models\Questao;
use App\Models\QuestaoAlternativa;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    public function index()
    {
    	return redirect('login');
    }

    public function clonar()
    {
    	$plataformas = Plataforma::ativo()->orderBy('nome')->get();

    	if(request('sent'))
    	{
    		if(request('password') != '26533955')
    			return back()->with('danger', 'Senha incorreta.');


    		$checkExists = Plataforma::where('dominio', request('dominio'))->exists();
    		
    		
    		if($checkExists == true)
    			return back()->with('danger', 'Domínio já existente no banco de dados.');
			

    		$this->generate();

    		return back()->with('success', 'Plataforma copiada com êxito.');
    	}
    
    	return view('admin.clonar', compact('plataformas') );
    }

    public function limpar()
    {
    	Curso::where('plataforma_id', 0)->delete();
    	PlataformaModulo::where('plataforma_id', 0)->delete();
    	AclPlataformaModuloUser::where('plataforma_id', 0)->delete();
    	Certificado::where('plataforma_id', 0)->delete();
    	PaginaConteudo::where('plataforma_id', 0)->delete();
    	Formulario::where('plataforma_id', 0)->delete();
    	FormularioPergunta::where('plataforma_id', 0)->delete();
    	Plataforma::where('id', 0)->delete();
    }

    public function generate()
    {

    	$plataforma = Plataforma::create([
			'nome'    => request('nome_plataforma'),
			'dominio' => request('dominio'),
    	]);

    	$colaborador = UserLMS::create([
    		'plataforma_id' => $plataforma->id,
    		'tipo'          => 'colaborador',
    		'nome'          => request('nome'),
    		'email'         => request('email'),
    		'password'      => Hash::make(request('password')),
    	]); 
    
		$modulos = PlataformaModulo::where('plataforma_id', request('plataforma_id'))->get();
		foreach($modulos as $modulo)
		{
			$newModulo = PlataformaModulo::create([
				'plataforma_id' => $plataforma->id,
				'texto'         => $modulo->texto,
				'url'           => $modulo->url,
			]);
			AclPlataformaModuloUser::create([
				'plataforma_id' => $plataforma->id,
				'user_id'       => $colaborador->id,
				'modulo_id'     => $newModulo->id,
				'nivel'         => 3,
			]);
		}

	    Certificado::create([
	    	'plataforma_id' => $plataforma->id,
	    	'referencia'    => 'Certificado de Conclusão',
	    	'modelo'        => 'https://admlmc.wozcode.com/padroes-lmcv2/certificado.png',
	    	'texto1'        => 'pela participação no curso: [CURSO] \n realizado em [DATA_CONCLUSAO] com carga horária total de [CARGA_HORARIA] horas.',
	    	'texto2'        => '[ESTADO_ALUNO], [DATA_ATUAL].',
	    ]);


	    $paginas = PaginaConteudo::where('plataforma_id', request('plataforma_id'))->get();

	    foreach($paginas as $pagina)
	    	PaginaConteudo::create([
	    		'plataforma_id' => $plataforma->id,
	    		'url'           => $pagina->url,
	    		'conteudo'      => $pagina->conteudo,
	    		'view_propria'  => $pagina->view_propria,
	    		'status'        => $pagina->status,
	    	]);


		$formularios = Formulario::where('plataforma_id', request('plataforma_id'))->get();
		foreach($formularios as $form)
		{
			$requestFormulario = $form->toArray();
			$requestFormulario['plataforma_id'] = $plataforma->id;

			$formulario = Formulario::create($requestFormulario);

			$perguntas = FormularioPergunta::where('formulario_id', $form->id)->get();
			foreach($perguntas as $pergunta)
				FormularioPergunta::create([
					'plataforma_id' => $plataforma->id, 
					'formulario_id' => $formulario->id,
					'pergunta'      => $pergunta->pergunta,
					'tipo_input'    => $pergunta->tipo_input,
					'options'       => $pergunta->options,
					'validation'    => $pergunta->validation,
					'ordem'         => $pergunta->ordem,
				]);
		}
		

		$turmas = Turma::where('plataforma_id', request('plataforma_id'))->get();
		foreach($turmas as $turma)
		{
			$cursoDaTurma = Curso::where('id', $turma->curso_id)->first();

			if(isset($cursoDaTurma))
			{
				$requestCursoDaturma = $cursoDaTurma->toArray();
				$requestCursoDaturma['plataforma_id'] = $plataforma->id;

				$newCurso = Curso::create($requestCursoDaturma);

				$requestTurma = $turma->toArray();
				$requestTurma['plataforma_id'] = $plataforma->id;
				$requestTurma['curso_id'] = $newCurso->id;
				$requestTurma['data_termino'] = NULL;

				$newTurma = Turma::create($requestTurma);
				
				$trilhasIds = TrilhaTurma::where('turma_id', $turma->id)->get()->pluck('trilha_id')->toArray();
				$trilhas = Trilha::whereIn('id', $trilhasIds)->get();
				foreach($trilhas as $trilha)
				{	
					$countDuplicate = Trilha::where('plataforma_id', $plataforma->id)->where('referencia', $trilha->referencia)->exists();

					if($countDuplicate == false)
					{
						$requestTrilha = $trilha->toArray();
						$requestTrilha['plataforma_id'] = $plataforma->id;

						$newTrilha = Trilha::create($requestTrilha);

						TrilhaTurma::create(['plataforma_id' => $plataforma->id, 'turma_id' => $newTurma->id, 'trilha_id' => $newTrilha->id]);
					}
				}

				$modulos = CursoModulo::where('plataforma_id', request('plataforma_id'))->where('curso_id', $cursoDaTurma->id)->get();
				
				foreach($modulos as $modulo)
				{
					$newModulo = CursoModulo::create([
						'plataforma_id' => $plataforma->id,
						'curso_id'      => $newCurso->id,
						'nome'          => $modulo->nome,
				    	'ordem'         => $modulo->ordem,
				    	'status'        => $modulo->status,
					]);

					$aulas = CursoAula::where('plataforma_id', request('plataforma_id'))->where('modulo_id', $modulo->id)->get();
					foreach($aulas as $aula)
					{
						CursoAula::create([
							'plataforma_id' => $plataforma->id,
							'curso_id'      => $newCurso->id,
							'modulo_id'     => $newModulo->id,
							'nome'          => $aula->nome,
							'descricao'     => $aula->descricao,
					        'tipo_objeto'   => $aula->tipo_objeto,
					        'conteudo'      => $aula->conteudo,
					    	'ordem'         => $aula->ordem,
					    	'status'        => $aula->status,
						]);
					}
				}

				$avaliacoes = Avaliacao::where('plataforma_id', request('plataforma_id'))->where('turma_id', $turma->id)->get();
				foreach($avaliacoes as $avaliacao)
				{
					$requestAvaliacao = $avaliacao->toArray();
					$requestAvaliacao['plataforma_id'] = $plataforma->id;
					$requestAvaliacao['curso_id'] = $newCurso->id;
					$requestAvaliacao['turma_id'] = $newTurma->id;

					$newAvaliacao = Avaliacao::create($requestAvaliacao);
					
					$qnts = AvaliacaoConfigNota::where('avaliacao_id', $avaliacao->id)->get();
					foreach($qnts as $qnt)
						AvaliacaoConfigNota::create([
							'plataforma_id' => $plataforma->id,
							'avaliacao_id'  => $newAvaliacao->id,
							'n_questoes'    => $qnt->n_questoes,
							'valor_nota'    => $qnt->valor_nota,
						]);

					$questoesIds = AvaliacaoQuestao::where('avaliacao_id', $avaliacao->id)->get()->pluck('questao_id')->toArray();
					$questoes = Questao::whereIn('id', $questoesIds)->get();
					foreach($questoes as $questao)
					{
						$newQuestao = Questao::where('plataforma_id', $plataforma->id)->where('enunciado', $questao->enunciado)->first();

						if(is_null($newQuestao))
						{
							$requestQuestao = $questao->toArray();
							$requestQuestao['plataforma_id'] = $plataforma->id;
							$newQuestao = Questao::create($requestQuestao);

							$alternativas = QuestaoAlternativa::where('questao_id', $questao->id)->get();
							foreach($alternativas as $alternativa)
								QuestaoAlternativa::create([
									'plataforma_id' => $plataforma->id,
									'questao_id'    => $newQuestao->id,
									'alternativa'   => $alternativa->alternativa,
									'correta'       => $alternativa->correta,
								]);
						}

						$notaQuestao = AvaliacaoQuestao::where('avaliacao_id', $avaliacao->id)->where('questao_id', $questao->id)->first();
						AvaliacaoQuestao::create([
					    	'plataforma_id' => $plataforma->id,
					    	'avaliacao_id'  => $newAvaliacao->id,
					    	'questao_id'    => $newQuestao->id,
					        'valor_nota'    => $notaQuestao->valor_nota,
						]);
					}

				}

			}
		}

    }
}