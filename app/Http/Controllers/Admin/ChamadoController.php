<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chamado;

class ChamadoController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Chamados';

        return view('admin.chamado.lista', compact('getNomePaginaInterno') );
    }
    
    public function info($chamado_id)
    {
        $getNomePaginaInterno = 'Chamado';

        return view('admin.chamado.info', compact('chamado_id','getNomePaginaInterno') );
    }

}