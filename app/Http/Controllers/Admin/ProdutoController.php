<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;

class ProdutoController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Produtos';

        return view('admin.produto.lista', compact('getNomePaginaInterno') );
    }
    
    public function info($produto_id)
    {
        $getNomePaginaInterno = 'Produto';

        return view('admin.produto.info', compact('produto_id','getNomePaginaInterno') );
    }

    public function add()
    {
        $getNomePaginaInterno = 'Adicionar Produto';

        return view('admin.produto.add', compact('getNomePaginaInterno') );
    }

    public function editar($produto_id)
    {
        $getNomePaginaInterno = 'Editar Produto';

        return view('admin.produto.editar', compact('produto_id','getNomePaginaInterno') );
    }

}