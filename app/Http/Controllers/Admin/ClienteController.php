<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cliente;

class ClienteController extends Controller
{
    public function lista()
    {
        $getNomePaginaInterno = 'Clientes';

        return view('admin.cliente.lista', compact('getNomePaginaInterno') );
    }
    
    public function info($cliente_id)
    {
        $getNomePaginaInterno = 'Cliente';

        return view('admin.cliente.info', compact('cliente_id','getNomePaginaInterno') );
    }

    public function add()
    {
        $getNomePaginaInterno = 'Adicionar Cliente';

        return view('admin.cliente.add', compact('getNomePaginaInterno') );
    }

    public function editar($cliente_id)
    {
        $getNomePaginaInterno = 'Editar Cliente';

        return view('admin.cliente.editar', compact('cliente_id','getNomePaginaInterno') );
    }

}