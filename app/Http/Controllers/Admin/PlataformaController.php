<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plataforma;
use App\Models\UserLMS;

class PlataformaController extends Controller
{
    public function info($id)
    {
    	$getNomePaginaInterno = 'Plataforma';

        $plataforma = Plataforma::findOrFail($id);

        $countAlunosAtivos = UserLMS::where('plataforma_id', $plataforma->id)->aluno()->ativo()->count();

        $responsavel = UserLMS::where('id', $plataforma->responsavel_id ?? 0)->first();

        if(request('sent'))
        {
        	$plataforma->update(['status' => request('status')]);

        	return back()->with('success', 'Status da plataforma atualizado com êxito.');
        }

        return view('admin.plataforma.info', compact('getNomePaginaInterno','responsavel','plataforma','countAlunosAtivos') );
    }
}
