<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plataforma;

class DashboardController extends Controller
{
    public function dashboard()
    {
    	$getNomePaginaInterno = 'Dashboard';

        $status = $_GET['status'] ?? array(0);

        $plataformas = Plataforma::whereIn('status', $status)->orderBy('id','desc')->get();

        return view('admin.dashboard', compact('getNomePaginaInterno','plataformas','status') );
    }
}
