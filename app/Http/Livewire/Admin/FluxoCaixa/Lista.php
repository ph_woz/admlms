<?php

namespace App\Http\Livewire\Admin\FluxoCaixa;

use Livewire\Component;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use App\Models\Util;
use App\Models\User;
use App\Models\FluxoCaixa;
use App\Models\Plataforma;
use App\Models\Cliente;
use App\Models\Produto;

class Lista extends Component
{
    public $buscar;
    public $mensalista;
    public $cliente_id;
    public $filter_cliente_id;
    public $produto_id;
	public $ano;
	public $mes;
    public $status;


    // ADD ENTRADA
    public $add_entrada_cliente_id;
    public $add_entrada_cliente_nome;
    public $add_entrada_cliente_site;
    public $add_entrada_ref_entrada;
    public $add_entrada_cliente_selected_nome;
    public $add_entrada_produto_id;
    public $add_entrada_produto_nome;
    public $add_entrada_valor_total;
    public $add_entrada_valor;
    public $add_entrada_n_parcelas;
    public $add_entrada_forma_pagamento;
    public $add_entrada_valueDisplayParcelas = 'none';
    public $add_entrada_data_recebimento;
    public $add_entrada_data_vencimento;
    public $add_entrada_data_estorno;
    public $add_entrada_status = 0;
    public $add_entrada_obs;

    public $add_entrada_n_parcela        = [];
    public $add_entrada_parcelas         = [];
    public $add_entrada_datas_vencimento = [];
    public $add_entrada_valores          = [];
    public $add_entrada_formas_pagamento = [];


    // EDITAR ENTRADA
    public $editar_entrada_cliente_id;
    public $editar_entrada_cliente_nome;
    public $editar_entrada_ref_entrada;
    public $editar_entrada_cliente_selected_nome;
    public $editar_entrada_produto_id;
    public $editar_entrada_produto_nome;
    public $editar_entrada_valor_total;
    public $editar_entrada_valor;
    public $editar_entrada_n_parcelas;
    public $editar_entrada_forma_pagamento;
    public $editar_entrada_valueDisplayParcelas = 'none';
    public $editar_entrada_data_recebimento;
    public $editar_entrada_data_vencimento;
    public $editar_entrada_id_para_acao;
    public $editar_entrada_data_estorno;
    public $editar_entrada_status = 0;
    public $editar_entrada_obs;
    public $editar_entrada;
    public $editar_entrada_id;

    public $editar_entrada_n_parcela        = [];
    public $editar_entrada_parcelas         = [];
    public $editar_entrada_datas_vencimento = [];
    public $editar_entrada_valores          = [];
    public $editar_entrada_formas_pagamento = [];
    public $editar_entrada_ids              = [];
    public $editar_entrada_registro_ids     = [];


    // ADD SAÍDA
    public $add_saida_ref_saida;
    public $add_saida_valor_total;
    public $add_saida_valor;
    public $add_saida_n_parcelas;
    public $add_saida_forma_pagamento;
    public $add_saida_valueDisplayParcelas = 'none';
    public $add_saida_data_recebimento;
    public $add_saida_data_vencimento;
    public $add_saida_status = 0;
    public $add_saida_obs;

    public $add_saida_n_parcela        = [];
    public $add_saida_parcelas         = [];
    public $add_saida_datas_vencimento = [];
    public $add_saida_valores          = [];
    public $add_saida_formas_pagamento = [];


    // EDITAR SAÍDA
    public $editar_saida;
    public $editar_saida_ref_saida;
    public $editar_saida_valor_total;
    public $editar_saida_valor;
    public $editar_saida_n_parcelas;
    public $editar_saida_forma_pagamento;
    public $editar_saida_valueDisplayParcelas = 'none';
    public $editar_saida_data_recebimento;
    public $editar_saida_data_vencimento;
    public $editar_saida_status = 0;
    public $editar_saida_obs;
    public $editar_saida_id_para_acao;

    public $editar_saida_ids              = [];
    public $editar_saida_registro_ids     = [];
    public $editar_saida_n_parcela        = [];
    public $editar_saida_parcelas         = [];
    public $editar_saida_datas_vencimento = [];
    public $editar_saida_valores          = [];
    public $editar_saida_formas_pagamento = [];


    // INFO ENTRADA
    public $view_entrada = true;
    public $entrada_id;

    // INFO SAIDA
    public $view_saida = true;
    public $saida_id;


    // DELETE
    public $password_to_delete;


    protected $queryString = ['buscar','produto_id','mes','ano','mensalista','filter_cliente_id','cliente_id','entrada_id','status'];


    public function mount()
    {
        $this->ano = $_GET['ano'] ?? date('Y');
        $this->mes = $_GET['mes'] ?? date('m');

        $this->cliente_id = $_GET['cliente_id'] ?? null;

        $this->add_entrada_n_parcelas = 1;
        $this->add_entrada_data_vencimento = date('Y-m-d');

        $this->add_saida_n_parcelas = 1;
        $this->add_saida_data_vencimento = date('Y-m-d');

        if(isset($_GET['entrada_id']) && $_GET['entrada_id'] != null)
        {
            $this->view_entrada = FluxoCaixa::plataforma()->where('id', $_GET['entrada_id'])->first() ?? false;
        }

        if(isset($_GET['saida_id']) && $_GET['saida_id'] != null)
        {
            $this->view_saida = FluxoCaixa::plataforma()->where('id', $_GET['saida_id'])->first() ?? false;
        }

        if(isset($_GET['editar_entrada_id']) && $_GET['editar_entrada_id'] != null)
        {
            $this->editar_entrada = FluxoCaixa::plataforma()->where('id', $_GET['editar_entrada_id'])->first() ?? false;

            if(isset($this->editar_entrada) && $this->editar_entrada !== false)
            {
                $this->editar_entrada_cliente_id = $this->editar_entrada->cliente_id;
                $this->editar_entrada_cliente_nome = $this->editar_entrada->cliente_nome;
                $this->editar_entrada_ref_entrada = $this->editar_entrada->ref_saida;
                $this->editar_entrada_cliente_selected_nome = $this->editar_entrada->cliente_nome;
                $this->editar_entrada_produto_id = $this->editar_entrada->produto_id;
                $this->editar_entrada_produto_nome = $this->editar_entrada->produto_nome;
                $this->editar_entrada_valor = $this->editar_entrada->valor;
                $this->editar_entrada_valor_total = $this->editar_entrada->valor_total;
                $this->editar_entrada_n_parcelas = $this->editar_entrada->n_parcelas;
                $this->editar_entrada_forma_pagamento = $this->editar_entrada->forma_pagamento;
                $this->editar_entrada_data_recebimento = $this->editar_entrada->data_recebimento;
                $this->editar_entrada_data_vencimento = $this->editar_entrada->data_vencimento;
                $this->editar_entrada_data_estorno = $this->editar_entrada->data_estorno;
                $this->editar_entrada_status = $this->editar_entrada->status;
                $this->editar_entrada_obs = $this->editar_entrada->obs;

                if($this->editar_entrada->n_parcelas >= 2)
                {            
                    $value = intval($this->editar_entrada->n_parcelas);

                    if($value >= 2)
                    {
                        $this->editar_entrada_valueDisplayParcelas = 'block';

                        if($this->editar_entrada->registro_id != null)
                        {
                            $this->editar_entrada_parcelas = FluxoCaixa::plataforma()->where('id', $this->editar_entrada->registro_id)->orWhere('registro_id', $this->editar_entrada->registro_id)->orderBy('data_vencimento')->get();
                        
                        } else {

                            $this->editar_entrada_parcelas = FluxoCaixa::plataforma()->where('registro_id', $this->editar_entrada->id)->orderBy('data_vencimento')->get();                            
                        }
                                                
                        $i = -1;
                        foreach($this->editar_entrada_parcelas as $parcela)
                        {
                            $i++;

                            $this->editar_entrada_n_parcela[$i]        = $parcela->n_parcela;
                            $this->editar_entrada_datas_vencimento[$i] = $parcela->data_vencimento;
                            $this->editar_entrada_valores[$i]          = $parcela->valor;
                            $this->editar_entrada_formas_pagamento[$i] = $parcela->forma_pagamento;
                            $this->editar_entrada_registro_ids[$i]     = $parcela->registro_id;
                            $this->editar_entrada_ids[$i]              = $parcela->id;
                        }

                    } else {

                        $this->editar_entrada_valueDisplayParcelas = 'none';
                    }
                }

            }
        }

        if(isset($_GET['editar_saida_id']) && $_GET['editar_saida_id'] != null)
        {
            $this->editar_saida = FluxoCaixa::plataforma()->where('id', $_GET['editar_saida_id'])->first() ?? false;

            if(isset($this->editar_saida) && $this->editar_saida !== false)
            {
                $this->editar_saida_ref_saida = $this->editar_saida->ref_saida;
                $this->editar_saida_valor = $this->editar_saida->valor;
                $this->editar_saida_valor_total = $this->editar_saida->valor_total;
                $this->editar_saida_n_parcelas = $this->editar_saida->n_parcelas;
                $this->editar_saida_forma_pagamento = $this->editar_saida->forma_pagamento;
                $this->editar_saida_data_recebimento = $this->editar_saida->data_recebimento;
                $this->editar_saida_data_vencimento = $this->editar_saida->data_vencimento;
                $this->editar_saida_status = $this->editar_saida->status;
                $this->editar_saida_obs = $this->editar_saida->obs;

                if($this->editar_saida->n_parcelas >= 2)
                {            
                    $value = intval($this->editar_saida->n_parcelas);

                    if($value >= 2)
                    {
                        $this->editar_saida_valueDisplayParcelas = 'block';

                        if($this->editar_saida->registro_id != null)
                        {
                            $this->editar_saida_parcelas = FluxoCaixa::plataforma()->where('id', $this->editar_saida->registro_id)->orWhere('registro_id', $this->editar_saida->registro_id)->orderBy('data_vencimento')->get();
                        
                        } else {

                            $this->editar_saida_parcelas = FluxoCaixa::plataforma()->where('registro_id', $this->editar_saida->id)->orderBy('data_vencimento')->get();                            
                        }
                                                
                        $i = -1;
                        foreach($this->editar_saida_parcelas as $parcela)
                        {
                            $i++;

                            $this->editar_saida_n_parcela[$i]        = $parcela->n_parcela;
                            $this->editar_saida_datas_vencimento[$i] = $parcela->data_vencimento;
                            $this->editar_saida_valores[$i]          = $parcela->valor;
                            $this->editar_saida_formas_pagamento[$i] = $parcela->forma_pagamento;
                            $this->editar_saida_registro_ids[$i]     = $parcela->registro_id;
                            $this->editar_saida_ids[$i]              = $parcela->id;
                        }

                    } else {

                        $this->editar_saida_valueDisplayParcelas = 'none';
                    }
                }

            }
        }
        if($this->cliente_id != null)
        {
            $this->cliente = UserLMS::plataforma()->aluno()->where('id', $this->cliente_id)->select('nome','email','cpf','celular')->first();

            if(isset($this->cliente))
            {
                $this->add_entrada_cliente_selected_nome = $this->cliente->nome . ' / ' . $this->cliente->email;

                if(isset($this->cliente->cpf))
                {
                    $this->add_entrada_cliente_selected_nome = $this->add_entrada_cliente_selected_nome . ' / ' . $this->cliente->cpf;
                }
                if(isset($this->cliente->celular))
                {
                    $this->add_entrada_cliente_selected_nome = $this->add_entrada_cliente_selected_nome . ' / ' . $this->cliente->celular;
                }
            }
        }
    }

    public function render()
    {
        if($this->cliente_id != null && is_null($this->cliente))
        {
            $erro = 'Aluno(a) não encontrado.';
            return view('admin.page-whoops', ['erro' => $erro]);
        }

    	$meses = Util::getMeses();

        $entradas = FluxoCaixa::plataforma()->where('tipo','entrada');
            
        $saidas = FluxoCaixa::plataforma()->where('tipo','saida');


        if($this->buscar)
        {
            $entradas->where(function ($query)
            {
                $query
                    ->orWhere('fluxo_caixa.ref_saida', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.cliente_nome', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.valor', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.forma_pagamento', 'like', '%'.$this->buscar.'%');
            });

            $saidas->where(function ($query)
            {
                $query
                    ->orWhere('fluxo_caixa.ref_saida', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.valor', 'like', '%'.$this->buscar.'%')
                    ->orWhere('fluxo_caixa.forma_pagamento', 'like', '%'.$this->buscar.'%');
            });
        }

        if($this->ano)
        {
            $entradas = $entradas->whereYear('fluxo_caixa.data_vencimento', $this->ano);
            $saidas   = $saidas->whereYear('fluxo_caixa.data_vencimento', $this->ano);
        }
        
        if($this->mes)
        {
            $entradas = $entradas->whereMonth('fluxo_caixa.data_vencimento', $this->mes);
            $saidas   = $saidas->whereMonth('fluxo_caixa.data_vencimento', $this->mes);
        }

        if($this->filter_cliente_id)
        {
            $entradas = $entradas->where('fluxo_caixa.cliente_id', $this->filter_cliente_id);
        }
        
        if($this->produto_id)
        {
            $entradas = $entradas->where('fluxo_caixa.produto_id', $this->produto_id);
        }

        if($this->status)
        {
            if($this->status == 'a-receber')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','>=', date('Y-m-d'));
            }

            if($this->status == 'recebidos-e-pagos')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 1);
                $saidas   = $saidas->where('fluxo_caixa.status', 1);
            }

            if($this->status == 'inadimplentes')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
                $saidas   = $saidas->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'));
            }

            if($this->status == 'estornados')
            {
                $entradas = $entradas->where('fluxo_caixa.status', 2);
                $saidas   = $saidas->where('fluxo_caixa.status', 2);
            }
        }

        if($this->mensalista)
        {
            $entradas = $entradas->where('fluxo_caixa.mensalista', $this->mensalista);
        }

        $inadimplentes = clone $entradas;
        $inadimplentes = $inadimplentes->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->select('valor')->get()->pluck('valor')->toArray();

        $despesasEmAtraso = clone $saidas;
        $countDespesasEmAtraso = $despesasEmAtraso->where('fluxo_caixa.status', 0)->whereDate('fluxo_caixa.data_vencimento','<', date('Y-m-d'))->count();

        $entradas = $entradas->orderBy('fluxo_caixa.id','desc')->get();
        $saidas   = $saidas->orderBy('fluxo_caixa.id','desc')->get();

        $clientes = Cliente::ativo()->orderBy('nome')->get();
        $produtos = Produto::ativo()->orderBy('nome')->get();

        return view('livewire.admin.fluxo-caixa.lista', 
        	[
        		'meses'                 => $meses,
                'entradas'              => $entradas,
                'saidas'                => $saidas,
                'inadimplentes'         => $inadimplentes,
                'countDespesasEmAtraso' => $countDespesasEmAtraso,
                'clientes'              => $clientes,
                'produtos'              => $produtos
        	]
        );
    }

    public function gerarParcelasAddEntrada($value)
    {
        $this->add_entrada_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->add_entrada_valueDisplayParcelas = 'block';

            $this->add_entrada_valor_total = $this->add_entrada_valor;

            $valor = $this->add_entrada_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->add_entrada_parcelas[$i] = null;
                $this->add_entrada_valores[$i]  = $valor;
                $this->add_entrada_valor = $valor;

                $this->add_entrada_formas_pagamento[$i] = $this->add_entrada_forma_pagamento;
                    
                $this->add_entrada_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->add_entrada_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->add_entrada_data_vencimento) )) ;
            }

        } else {

            $this->add_entrada_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasEditarEntrada($value)
    {
        $this->editar_entrada_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->editar_entrada_valueDisplayParcelas = 'block';

            $valor = $this->editar_entrada_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->editar_entrada_parcelas[$i] = null;
                $this->editar_entrada_valores[$i]  = $valor;
                $this->editar_entrada_valor = $valor;

                $this->editar_entrada_formas_pagamento[$i] = $this->editar_entrada_forma_pagamento;
                    
                $this->editar_entrada_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->editar_entrada_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->editar_entrada_data_vencimento) )) ;
            }

        } else {

            $this->editar_entrada_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasAddSaida($value)
    {
        $this->add_saida_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->add_saida_valueDisplayParcelas = 'block';

            $this->add_saida_valor_total = $this->add_saida_valor;

            $valor = $this->add_saida_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->add_saida_parcelas[$i] = null;
                $this->add_saida_valores[$i]  = $valor;
                $this->add_saida_valor = $valor;

                $this->add_saida_formas_pagamento[$i] = $this->add_saida_forma_pagamento;
                    
                $this->add_saida_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->add_saida_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->add_saida_data_vencimento) )) ;
            }

        } else {

            $this->add_saida_valueDisplayParcelas = 'none';
        }
    }

    public function gerarParcelasEditarSaida($value)
    {
        $this->editar_saida_parcelas = [];

        $value = intval($value);

        if($value >= 2)
        {
            $this->editar_saida_valueDisplayParcelas = 'block';

            $valor = $this->editar_saida_valor;
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            $valor = floatval($valor);
            $valor = round($valor, 2);
            $valor = $valor / $value;
            $valor = number_format($valor , 2,",",".");


            for ($i=1; $i < $value; $i++)
            { 
                $this->editar_saida_parcelas[$i] = null;
                $this->editar_saida_valores[$i]  = $valor;
                $this->editar_saida_valor = $valor;

                $this->editar_saida_formas_pagamento[$i] = $this->editar_saida_forma_pagamento;
                    
                $this->editar_saida_n_parcela[$i] = $i+1;

                $interval = '+'.$i.' month';
                $this->editar_saida_datas_vencimento[$i] = date('Y-m-d', strtotime ($interval , strtotime ($this->editar_saida_data_vencimento) )) ;
            }

        } else {

            $this->editar_saida_valueDisplayParcelas = 'none';
        }
    }

    public function addEntrada()
    {
        $entrada = FluxoCaixa::create([
            'cadastrante_id'   => \Auth::id(),
            'cadastrante_nome' => \Auth::user()->nome,
            'ref_saida'        => $this->add_entrada_ref_entrada,
            'cliente_id'       => $this->add_entrada_cliente_id,
            'cliente_nome'     => $this->add_entrada_cliente_nome,
            'produto_id'       => $this->add_entrada_produto_id,
            'produto_nome'     => $this->add_entrada_produto_nome,
            'tipo'             => 'entrada',
            'valor'            => $this->add_entrada_valor,
            'forma_pagamento'  => $this->add_entrada_forma_pagamento,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->add_entrada_n_parcelas,
            'data_recebimento' => $this->add_entrada_data_recebimento,
            'data_vencimento'  => $this->add_entrada_data_vencimento,
            'data_estorno'     => $this->add_entrada_data_estorno,
            'status'           => $this->add_entrada_status,
            'obs'              => $this->add_entrada_obs,
        ]);
    

        $lineParcelas = array_map(function ($add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento)
        {
          return array_combine(
            ['add_entrada_n_parcela','add_entrada_data_vencimento','add_entrada_valor', 'add_entrada_forma_pagamento'],
            [$add_entrada_n_parcela, $add_entrada_data_vencimento, $add_entrada_valor, $add_entrada_forma_pagamento]
          );
        }, $this->add_entrada_n_parcela, $this->add_entrada_datas_vencimento, $this->add_entrada_valores, $this->add_entrada_formas_pagamento);

        $valores = [];
        $valores[] = $this->add_entrada_valor;

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_entrada_valor'];

            FluxoCaixa::create([
                'cadastrante_id'   => \Auth::id(),
                'cadastrante_nome' => \Auth::user()->nome,
                'ref_saida'        => $this->add_entrada_ref_entrada,
                'cliente_id'       => $this->add_entrada_cliente_id,
                'cliente_nome'     => $this->add_entrada_cliente_nome,
                'produto_nome'     => $this->add_entrada_produto_nome,
                'produto_id'       => $this->add_entrada_produto_id,
                'registro_id'      => $entrada->id,
                'tipo'             => 'entrada',
                'n_parcelas'       => $this->add_entrada_n_parcelas,
                'n_parcela'        => $parcela['add_entrada_n_parcela'],
                'valor'            => $parcela['add_entrada_valor'],
                'forma_pagamento'  => $parcela['add_entrada_forma_pagamento'],
                'data_vencimento'  => $parcela['add_entrada_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        
        $entrada->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $entrada->id)->update(['valor_total' => $valor_total]);

        $this->resetFieldsAddEntrada();

        session()->flash('success-livewire', 'Entrada cadastrada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function editarEntrada()
    {
        $this->editar_entrada->update([
            'editor_id'        => \Auth::id(),
            'editor_nome'      => \Auth::user()->nome,
            'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
            'cliente_id'       => $this->editar_entrada_cliente_id,
            'produto_nome'     => $this->editar_entrada_produto_nome,
            'produto_id'       => $this->editar_entrada_produto_id,
            'ref_saida'        => $this->editar_entrada_ref_entrada,
            'tipo'             => 'entrada',
            'valor'            => $this->editar_entrada_valor,
            'forma_pagamento'  => $this->editar_entrada_forma_pagamento,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->editar_entrada_n_parcelas,
            'data_recebimento' => $this->editar_entrada_data_recebimento,
            'data_vencimento'  => $this->editar_entrada_data_vencimento,
            'data_estorno'     => $this->editar_entrada_data_estorno,
            'status'           => $this->editar_entrada_status,
            'obs'              => $this->editar_entrada_obs,
        ]);
    
        
        $lineParcelas = array_map(function ($editar_entrada_n_parcela, $editar_entrada_data_vencimento, $editar_entrada_valor, $editar_entrada_forma_pagamento, $editar_entrada_registro_id, $editar_entrada_id)
        {
          return array_combine(
            ['editar_entrada_n_parcela','editar_entrada_data_vencimento','editar_entrada_valor', 'editar_entrada_forma_pagamento', 'editar_entrada_registro_id','editar_entrada_id'],
            [$editar_entrada_n_parcela, $editar_entrada_data_vencimento, $editar_entrada_valor, $editar_entrada_forma_pagamento, $editar_entrada_registro_id, $editar_entrada_id]
          );
        }, $this->editar_entrada_n_parcela, $this->editar_entrada_datas_vencimento, $this->editar_entrada_valores, $this->editar_entrada_formas_pagamento, $this->editar_entrada_registro_ids, $this->editar_entrada_ids);


        $parcelasIds = [];
        $parcelasIds[] = $this->editar_entrada->id;

        foreach($lineParcelas as $parcela)
        {
            $checkEntrada = FluxoCaixa::plataforma()->where('id', $parcela['editar_entrada_id'] ?? 0)->first();

            if($checkEntrada)
            {
                $checkEntrada->update([
                    'editor_id'        => \Auth::id(),
                    'editor_nome'      => \Auth::user()->nome,
                    'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
                    'produto_nome'     => $this->editar_entrada_produto_nome,
                    'n_parcelas'       => $this->editar_entrada_n_parcelas,
                    'n_parcela'        => $parcela['editar_entrada_n_parcela'],
                    'valor'            => $parcela['editar_entrada_valor'],
                    'forma_pagamento'  => $parcela['editar_entrada_forma_pagamento'],
                    'data_vencimento'  => $parcela['editar_entrada_data_vencimento'],
                ]);

            } else {

                $checkEntrada = FluxoCaixa::create([
                    'cadastrante_id'   => \Auth::id(),
                    'cadastrante_nome' => \Auth::user()->nome,
                    'cliente_id'       => $this->editar_entrada->cliente_id,
                    'cliente_nome'     => $this->editar_entrada_cliente_selected_nome,
                    'produto_nome'     => $this->editar_entrada_produto_nome,
                    'tipo'             => 'entrada',
                    'n_parcelas'       => $this->editar_entrada_n_parcelas,
                    'registro_id'      => $this->editar_entrada->registro_id ?? $this->editar_entrada->id,
                    'n_parcela'        => $parcela['editar_entrada_n_parcela'],
                    'valor'            => $parcela['editar_entrada_valor'],
                    'forma_pagamento'  => $parcela['editar_entrada_forma_pagamento'],
                    'data_vencimento'  => $parcela['editar_entrada_data_vencimento'],  
                ]);

            }

            FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->where('n_parcela', $checkEntrada->n_parcela)->where('registro_id', $checkEntrada->registro_id)->where('id','<>', $checkEntrada->id)->delete();

            $parcelasIds[] = $checkEntrada->id;
        }

        $valores = FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->whereIn('id', $parcelasIds)->select('valor')->get()->pluck('valor')->toArray();
        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        FluxoCaixa::plataforma()->where('cliente_id', $this->editar_entrada->cliente_id)->whereIn('id', $parcelasIds)->update(['valor_total' => $valor_total]);

        session()->flash('success-livewire', 'Entrada atualizada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function addSaida()
    {
        $saida = FluxoCaixa::create([
            'cadastrante_id'   => \Auth::id(),
            'cadastrante_nome' => \Auth::user()->nome,
            'ref_saida'        => $this->add_saida_ref_saida,
            'tipo'             => 'saida',
            'valor'            => $this->add_saida_valor,
            'forma_pagamento'  => $this->add_saida_forma_pagamento,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->add_saida_n_parcelas,
            'data_recebimento' => $this->add_saida_data_recebimento,
            'data_vencimento'  => $this->add_saida_data_vencimento,
            'status'           => $this->add_saida_status,
            'obs'              => $this->add_saida_obs,
        ]);
    

        $lineParcelas = array_map(function ($add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento)
        {
          return array_combine(
            ['add_saida_n_parcela','add_saida_data_vencimento','add_saida_valor', 'add_saida_forma_pagamento'],
            [$add_saida_n_parcela, $add_saida_data_vencimento, $add_saida_valor, $add_saida_forma_pagamento]
          );
        }, $this->add_saida_n_parcela, $this->add_saida_datas_vencimento, $this->add_saida_valores, $this->add_saida_formas_pagamento);

        $valores = [];
        $valores[] = $this->add_saida_valor;

        foreach($lineParcelas as $parcela)
        {
            $valores[] = $parcela['add_saida_valor'];

            FluxoCaixa::create([
                'cadastrante_id'   => \Auth::id(),
                'cadastrante_nome' => \Auth::user()->nome,
                'ref_saida'        => $this->add_saida_ref_saida,
                'registro_id'      => $saida->id,
                'tipo'             => 'saida',
                'n_parcelas'       => $this->add_saida_n_parcelas,
                'n_parcela'        => $parcela['add_saida_n_parcela'],
                'valor'            => $parcela['add_saida_valor'],
                'forma_pagamento'  => $parcela['add_saida_forma_pagamento'],
                'data_vencimento'  => $parcela['add_saida_data_vencimento'],
            ]);
        }

        $valor_total = FluxoCaixa::getValorRealInArray($valores);

        $saida->update(['valor_total' => $valor_total]);

        FluxoCaixa::plataforma()->where('registro_id', $saida->id)->update(['valor_total' => $valor_total]);

        $this->resetFieldsAddSaida();

        session()->flash('success-livewire', 'Saída cadastrada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function resetFieldsAddSaida()
    {
        $this->add_saida_ref_saida = null;
        $this->add_saida_valor = null;
        $this->add_saida_forma_pagamento = null;
        $this->add_saida_n_parcelas = 1;
        $this->add_saida_data_recebimento = null;
        $this->add_saida_data_vencimento = date('Y-m-d');
        $this->add_saida_status = 0;
        $this->add_saida_obs = null;
        $this->add_saida_cliente_id = null;
        $this->add_saida_produto_id = null;
        $this->add_saida_ref_entrada = null;
        $this->add_saida_parcelas = [];
        $this->add_saida_valueDisplayParcelas = 'none';
        $this->add_saida_parcelas = [];
        $this->add_saida_valores  = [];
        $this->add_saida_formas_pagamento = [];
        $this->add_saida_n_parcela = [];
        $this->add_saida_datas_vencimento = [];
    }

    public function resetFieldsAddEntrada()
    {
        $this->add_entrada_vendedor_id = null;
        $this->cliente_id = null;
        $this->add_entrada_cliente_selected_nome = null;
        $this->add_entrada_trilha_id = null;
        $this->add_entrada_turma_id = null;
        $this->add_entrada_produto_nome = null;
        $this->add_entrada_unidade_nome = null;
        $this->add_entrada_vendedor_nome = null;
        $this->add_entrada_unidade_id = null;
        $this->add_entrada_valor = null;
        $this->add_entrada_forma_pagamento = null;
        $this->add_entrada_n_parcelas = 1;
        $this->add_entrada_data_recebimento = null;
        $this->add_entrada_data_vencimento = date('Y-m-d');
        $this->add_entrada_data_estorno = null;
        $this->add_entrada_status = 0;
        $this->add_entrada_obs = null;
        $this->add_entrada_cliente_id = null;
        $this->add_entrada_produto_id = null;
        $this->add_entrada_ref_entrada = null;
        $this->add_entrada_parcelas = [];
        $this->add_entrada_valueDisplayParcelas = 'none';
        $this->add_entrada_parcelas = [];
        $this->add_entrada_valores  = [];
        $this->add_entrada_formas_pagamento = [];
        $this->add_entrada_n_parcela = [];
        $this->add_entrada_datas_vencimento = [];
    }

    public function editarSaida()
    {
        $this->editar_saida->update([
            'editor_id'        => \Auth::id(),
            'editor_nome'      => \Auth::user()->nome,
            'ref_saida'        => $this->editar_saida_ref_saida,
            'tipo'             => 'saida',
            'valor'            => $this->editar_saida_valor,
            'forma_pagamento'  => $this->editar_saida_forma_pagamento,
            'n_parcela'        => 1,
            'n_parcelas'       => $this->editar_saida_n_parcelas,
            'data_recebimento' => $this->editar_saida_data_recebimento,
            'data_vencimento'  => $this->editar_saida_data_vencimento,
            'status'           => $this->editar_saida_status,
            'obs'              => $this->editar_saida_obs,
        ]);
    
        
        $lineParcelas = array_map(function ($editar_saida_n_parcela, $editar_saida_data_vencimento, $editar_saida_valor, $editar_saida_forma_pagamento, $editar_saida_registro_id, $editar_saida_id)
        {
          return array_combine(
            ['editar_saida_n_parcela','editar_saida_data_vencimento','editar_saida_valor', 'editar_saida_forma_pagamento', 'editar_saida_registro_id','editar_saida_id'],
            [$editar_saida_n_parcela, $editar_saida_data_vencimento, $editar_saida_valor, $editar_saida_forma_pagamento, $editar_saida_registro_id, $editar_saida_id]
          );
        }, $this->editar_saida_n_parcela, $this->editar_saida_datas_vencimento, $this->editar_saida_valores, $this->editar_saida_formas_pagamento, $this->editar_saida_registro_ids, $this->editar_saida_ids);


        $parcelasIds = [];
        $parcelasIds[] = $this->editar_saida->id;

        foreach($lineParcelas as $parcela)
        {
            $checkSaida = FluxoCaixa::plataforma()->where('id', $parcela['editar_saida_id'] ?? 0)->first();

            if($checkSaida)
            {
                $checkSaida->update([
                    'editor_id'        => \Auth::id(),
                    'editor_nome'      => \Auth::user()->nome,
                    'ref_saida'        => $this->editar_saida_ref_saida,
                    'n_parcelas'       => $this->editar_saida_n_parcelas,
                    'n_parcela'        => $parcela['editar_saida_n_parcela'],
                    'valor'            => $parcela['editar_saida_valor'],
                    'forma_pagamento'  => $parcela['editar_saida_forma_pagamento'],
                    'data_vencimento'  => $parcela['editar_saida_data_vencimento'],
                ]);

            } else {

                $checkSaida = FluxoCaixa::create([
                    'cadastrante_id'   => \Auth::id(),
                    'cadastrante_nome' => \Auth::user()->nome,
                    'ref_saida'        => $this->editar_saida_ref_saida,
                    'tipo'             => 'saida',
                    'n_parcelas'       => $this->editar_saida_n_parcelas,
                    'registro_id'      => $this->editar_saida->registro_id ?? $this->editar_saida->id,
                    'n_parcela'        => $parcela['editar_saida_n_parcela'],
                    'valor'            => $parcela['editar_saida_valor'],
                    'forma_pagamento'  => $parcela['editar_saida_forma_pagamento'],
                    'data_vencimento'  => $parcela['editar_saida_data_vencimento'],  
                ]);

            }

            FluxoCaixa::plataforma()->where('n_parcela', $checkSaida->n_parcela)->where('registro_id', $checkSaida->registro_id)->where('id','<>', $checkSaida->id)->delete();

            $parcelasIds[] = $checkSaida->id;
        }

        $valores = FluxoCaixa::plataforma()->whereIn('id', $parcelasIds)->select('valor')->get()->pluck('valor')->toArray();
        $valor_total = FluxoCaixa::getValorRealInArray($valores);
        FluxoCaixa::plataforma()->whereIn('id', $parcelasIds)->update(['valor_total' => $valor_total]);

        session()->flash('success-livewire', 'Saída atualizada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');

    }


    public function setAddEntradaProdutoNome($value)
    {
        $nome = Produto::select('nome')->where('id', $value)->pluck('nome')[0] ?? null;

        $this->add_entrada_produto_nome = $nome;

        if($this->add_entrada_cliente_site)
        {
            $this->add_entrada_ref_entrada = $this->add_entrada_cliente_site;
        }
    }

    public function setAddEntradaClienteNome($value)
    {
        $cliente = Cliente::select('nome','site')->where('id', $value)->first();

        $this->add_entrada_cliente_nome = $cliente->nome;
        $this->add_entrada_cliente_site = $cliente->site;
    }

    public function setEditarEntradaClienteNome($value)
    {
        $nome = Cliente::select('nome')->where('id', $value)->pluck('nome')[0] ?? null;

        $this->editar_entrada_cliente_nome = $nome;
    }

    public function setEditarEntradaProdutoNome($value)
    {
        $nome = Produto::select('nome')->where('id', $value)->pluck('nome')[0] ?? null;

        $this->editar_entrada_produto_nome = $nome;
    }

    public function infoEntrada($entrada_id, $mes, $ano)
    {   
        if(strlen($mes) == 1)
        {
            $mes = '0'.$mes;
        }

        $_GET['mes'] = $mes;
        $_GET['ano'] = $ano;
        
        $this->view_entrada = true;
        $this->view_entrada = FluxoCaixa::plataforma()->where('id', $entrada_id)->first() ?? false;
        $this->entrada_id   = $entrada_id;
    }

    public function infoSaida($saida_id, $mes, $ano)
    {           
        if(strlen($mes) == 1)
        {
            $mes = '0'.$mes;
        }

        $_GET['mes'] = $mes;
        $_GET['ano'] = $ano;
        
        $this->view_saida = true;
        $this->view_saida = FluxoCaixa::plataforma()->where('id', $saida_id)->first() ?? false;
        $this->saida_id   = $saida_id;
    }

    public function setIdParaMarcarDespesaComoPaga($id)
    {
        $this->editar_saida_id_para_acao = $id;
        $this->editar_saida_data_recebimento = date('Y-m-d');
    }

    public function marcarDespesaComoPaga()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_saida_id_para_acao)->update(['status' => 1, 'data_recebimento' => $this->editar_saida_data_recebimento]);

        session()->flash('success-livewire', 'Despesa marcada como paga com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaMarcarVendaComoRecebida($id)
    {
        $this->editar_entrada_id_para_acao = $id;
        $this->editar_entrada_data_recebimento = date('Y-m-d');
    }

    public function marcarVendaComoRecebida()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->update(['status' => 1, 'data_recebimento' => $this->editar_entrada_data_recebimento]);

        session()->flash('success-livewire', 'Venda marcada como recebida com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaMarcarVendaComoEstornada($id)
    {
        $this->editar_entrada_id_para_acao = $id;
        $this->editar_entrada_data_estorno = date('Y-m-d');
    }

    public function marcarVendaComoEstornada()
    {
        FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->update(['status' => 2, 'data_estorno' => $this->editar_entrada_data_recebimento]);

        session()->flash('success-livewire', 'Venda marcada como estornada com êxito.');
        $this->dispatchBrowserEvent('dissmiss-modal');
        $this->dispatchBrowserEvent('toast');
    }

    public function setIdParaDeletarEntrada($id)
    {
        $this->editar_entrada_id_para_acao = $id;
    }

    public function deletarEntrada()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $entrada = FluxoCaixa::plataforma()->where('id', $this->editar_entrada_id_para_acao)->first();

            $deleteAndReturnCountDeleteds = 0;

            if($entrada->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->where('registro_id', $entrada->id)->delete();
            }

            $entrada->delete();

            session()->flash('success', 'Entrada deletada com êxito.');
            
            return redirect(route('admin.fluxo-caixa.lista', ['mes' => $this->mes, 'ano' => $this->ano]));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function deletarSaida()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            $saida = FluxoCaixa::plataforma()->where('id', $this->editar_saida_id_para_acao)->first();

            $deleteAndReturnCountDeleteds = 0;

            if($saida->registro_id == null)
            {
                $deleteAndReturnCountDeleteds = FluxoCaixa::plataforma()->where('registro_id', $saida->id)->delete();
            }

            $saida->delete();

            session()->flash('success', 'Saída deletada com êxito.');
            
            return redirect(route('admin.fluxo-caixa.lista', ['mes' => $this->mes, 'ano' => $this->ano]));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }

    public function setIdParaDeletarSaida($id)
    {
        $this->editar_saida_id_para_acao = $id;
    }

}
