<?php

namespace App\Http\Livewire\Admin\Produto;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Produto;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $mensalista;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','mensalista','ordem'];

    public function render()
    {
        $produtos = Produto::select('id','nome','mensalista','status');

        if($this->search)
        {
            $produtos->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%');
            });
        }

        if($this->mensalista)
        {
            $produtos = $produtos->where('mensalista', $this->mensalista);
        }

        $produtos = $produtos->where('status', $this->status);

        $countProdutos = $produtos->count();

        if($this->ordem == 'desc' || $this->ordem == 'asc')
            $produtos = $produtos->orderBy('id', $this->ordem);
        else
            $produtos = $produtos->orderBy('nome');


        $produtos = $produtos->paginate(25);


        return view('livewire.admin.produto.lista', [
            'produtos'       => $produtos,
            'countProdutos'  => $countProdutos,
        ]);
    }
}
