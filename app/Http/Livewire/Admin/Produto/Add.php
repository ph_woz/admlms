<?php

namespace App\Http\Livewire\Admin\Produto;

use Livewire\Component;
use App\Models\Produto;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $produto;
    public $nome; 
    public $mensalista = 'N'; 
    public $status = 0;
    public $descricao; 

    public function add()
    {
        $this->produto = Produto::create([
            'cadastrante_id' => \Auth::id(),
            'nome' => $this->nome,
            'mensalista' => $this->mensalista,
            'status' => $this->status,
            'descricao' => $this->descricao,
        ]);

        $this->resetFields();

        session()->flash('success-livewire', 'Produto criado com êxito.');
    }

    public function resetFields()
    {
        $this->nome = null;
        $this->mensalista = 'N';
        $this->status = 0;
        $this->descricao = null;
    }

    public function render()
    {
        return view('livewire.admin.produto.add');
    }

}
