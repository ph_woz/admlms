<?php

namespace App\Http\Livewire\Admin\Produto;

use Livewire\Component;
use App\Models\Produto;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $produto;
    public $nome; 
    public $mensalista; 
    public $status = 0;
    public $descricao; 

    public function mount($produto_id)
    {
        $this->produto = Produto::find($produto_id);

        $this->nome = $this->produto->nome;
        $this->mensalista = $this->produto->mensalista;
        $this->status = $this->produto->status;
        $this->descricao = $this->produto->descricao;
    }

    public function editar()
    {
        $this->produto->update([
            'cadastrante_id' => \Auth::id(),
            'nome' => $this->nome,
            'mensalista' => $this->mensalista,
            'status' => $this->status,
            'descricao' => $this->descricao,
        ]);

        session()->flash('success-livewire', 'Produto atualizado com êxito.');
    }

    public function render()
    {
        return view('livewire.admin.produto.editar');
    }

}
