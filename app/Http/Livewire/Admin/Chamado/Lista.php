<?php

namespace App\Http\Livewire\Admin\Chamado;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Chamado;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $status;
    public $arquivado = 'N';
    public $ordem = 'desc';

    protected $queryString = [
        'status',
        'arquivado',
        'ordem',
    ];


    public function render()
    {
        $chamados = Chamado::join('plataformas','chamados.plataforma_id','plataformas.id');

        if($this->status != null)
        {
            $chamados = $chamados->where('chamados.status', $this->status);
        }

        if($this->arquivado)
        {
            $chamados = $chamados->where('chamados.arquivado', $this->arquivado);
        }

        $countChamados = $chamados->count();

        $chamados = $chamados->select('plataformas.dominio','chamados.id','chamados.assunto','chamados.status',)->orderBy('chamados.id', $this->ordem)->paginate(25);

        return view('livewire.admin.chamado.lista', [
            'chamados'      => $chamados,
            'countChamados' => $countChamados,
        ]);
    }
}
