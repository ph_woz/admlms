<?php

namespace App\Http\Livewire\Admin\Chamado;

use Livewire\Component;
use App\Models\UserLMS;
use App\Models\Chamado;
use App\Models\ChamadoComentario;
use App\Models\Plataforma;
use Mail;
use App\Mail\EmailNotificaChamadoComentarioCadastrante;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $chamado;
    public $cadastrante;
    public $comentario_descricao;

    public function mount($chamado_id)
    {
        $this->chamado = Chamado::find($chamado_id);

        $this->cadastrante = UserLMS::where('id', $this->chamado->cadastrante_id)->first();
    }

    public function render()
    {
        $comentarios = ChamadoComentario::where('chamado_id', $this->chamado->id)->orderBy('id','desc')->get();

        $dominio = Plataforma::where('id', $this->chamado->plataforma_id)->select('dominio')->pluck('dominio')[0] ?? null;

        return view('livewire.admin.chamado.info', ['comentarios' => $comentarios, 'dominio' => $dominio]);
    }

    public function addComentario()
    {
        $comentario = ChamadoComentario::create([
            'plataforma_id' => $this->chamado->plataforma_id,
            'chamado_id'    => $this->chamado->id,
            'descricao'     => $this->comentario_descricao,
            'tipo_autor'    => 'W',
        ]);

        $this->comentario_descricao = null;

        session()->flash('success-livewire', 'Comentário adicionado com êxito.');
        $this->dispatchBrowserEvent('toast');

        Mail::to($this->cadastrante->email)->send(new EmailNotificaChamadoComentarioCadastrante($this->chamado->assunto, $comentario));
    }

    public function setStatus($status)
    {
        $this->chamado->update(['status' => $status]);

        session()->flash('success-livewire', 'Status do Chamado atualizado com êxito.');
        $this->dispatchBrowserEvent('toast');
    }

    public function setArquivar($arquivado)
    {
        $this->chamado->update(['arquivado' => $arquivado]);

        if($arquivado == 'S')
            $flash = 'Arquivado com êxito.';
        else
            $flash = 'Desarquivado com êxito.';
         
        session()->flash('success-livewire', $flash);
        $this->dispatchBrowserEvent('toast');
    }

}
