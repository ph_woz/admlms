<?php

namespace App\Http\Livewire\Admin\Cliente;

use Livewire\Component;
use App\Models\Cliente;
use Illuminate\Support\Facades\Http;

class Editar extends Component
{
    public $cliente;
    public $nome; 
    public $email; 
    public $status = 0;
    public $rg;
    public $cpf;
    public $cnpj;
    public $site;
    public $genero;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $obs;
    public $cep;
    public $rua;
    public $numero;
    public $bairro;
    public $cidade;
    public $estado;
    public $complemento;
    public $ponto_referencia;
    public $pais;

    public function mount($cliente_id)
    {
        $this->cliente = Cliente::find($cliente_id);

        $this->nome = $this->cliente->nome;
        $this->email = $this->cliente->email;
        $this->rg = $this->cliente->rg;
        $this->cpf = $this->cliente->cpf;
        $this->cnpj = $this->cliente->cnpj;
        $this->site = $this->cliente->site;
        $this->genero = $this->cliente->genero;
        $this->data_nascimento = $this->cliente->data_nascimento;
        $this->celular = $this->cliente->celular;
        $this->telefone = $this->cliente->telefone;
        $this->obs = $this->cliente->obs;
        $this->cep = $this->cliente->cep;
        $this->rua = $this->cliente->rua;
        $this->numero = $this->cliente->numero;
        $this->bairro = $this->cliente->bairro;
        $this->cidade = $this->cliente->cidade;
        $this->estado = $this->cliente->estado;
        $this->complemento = $this->cliente->complemento;
        $this->ponto_referencia = $this->cliente->ponto_referencia;
        $this->pais = $this->cliente->pais;

    }

    public function editar()
    {
        $this->cliente->update([
            'nome' => $this->nome,
            'email' => $this->email,
            'status' => $this->status,
            'rg' => $this->rg,
            'cpf' => $this->cpf,
            'cnpj' => $this->cnpj,
            'site' => $this->site,
            'genero' => $this->genero,
            'data_nascimento' => $this->data_nascimento,
            'celular' => $this->celular,
            'telefone' => $this->telefone,
            'obs' => $this->obs,
            'cep' => $this->cep,
            'rua' => $this->rua,
            'numero' => $this->numero,
            'bairro' => $this->bairro,
            'cidade' => $this->cidade,
            'estado' => $this->estado,
            'complemento' => $this->complemento,
            'ponto_referencia' => $this->ponto_referencia,
            'pais' => $this->pais,
        ]);

        session()->flash('success-livewire', 'Cliente atualizado com êxito.');
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->cep}/json/");
        $viacep = $response->json();
        $this->rua    = $viacep['logradouro'] ?? null;
        $this->bairro = $viacep['bairro'] ?? null;
        $this->cidade = $viacep['localidade'] ?? null;
        $this->estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        return view('livewire.admin.cliente.editar');
    }

}
