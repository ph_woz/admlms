<?php

namespace App\Http\Livewire\Admin\Cliente;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Cliente;

class Lista extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $status = 0;
    public $ordem;

    protected $queryString = ['search','status','ordem'];

    public function render()
    {
        $clientes = Cliente::select('id','nome','celular','status');

        if($this->search)
        {
            $clientes->where(function ($query)
            {
                $query
                    ->orWhere('nome', 'like', '%'.$this->search.'%')
                    ->orWhere('email', 'like', '%'.$this->search.'%')
                    ->orWhere('cpf', 'like', '%'.$this->search.'%');
            });
        }

        $clientes = $clientes->where('status', $this->status);

        $countClientes = $clientes->count();

        if($this->ordem == 'desc' || $this->ordem == 'asc')
            $clientes = $clientes->orderBy('id', $this->ordem);
        else
            $clientes = $clientes->orderBy('nome');


        $clientes = $clientes->paginate(25);


        return view('livewire.admin.cliente.lista', [
            'clientes'       => $clientes,
            'countClientes' => $countClientes,
        ]);
    }
}
