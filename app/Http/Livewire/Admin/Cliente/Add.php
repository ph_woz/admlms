<?php

namespace App\Http\Livewire\Admin\Cliente;

use Livewire\Component;
use App\Models\Cliente;
use Illuminate\Support\Facades\Http;

class Add extends Component
{
    public $cliente;
    public $nome; 
    public $email; 
    public $status = 0;
    public $rg;
    public $cpf;
    public $cnpj;
    public $site;
    public $genero;
    public $data_nascimento;
    public $celular;
    public $telefone;
    public $obs;
    public $cep;
    public $rua;
    public $numero;
    public $bairro;
    public $cidade;
    public $estado;
    public $complemento;
    public $ponto_referencia;
    public $pais;

    public function add()
    {
        $this->cliente = Cliente::create([
            'cadastrante_id' => \Auth::id(),
            'nome' => $this->nome,
            'email' => $this->email,
            'status' => $this->status,
            'rg' => $this->rg,
            'cpf' => $this->cpf,
            'cnpj' => $this->cnpj,
            'site' => $this->site,
            'genero' => $this->genero,
            'data_nascimento' => $this->data_nascimento,
            'celular' => $this->celular,
            'telefone' => $this->telefone,
            'obs' => $this->obs,
            'cep' => $this->cep,
            'rua' => $this->rua,
            'numero' => $this->numero,
            'bairro' => $this->bairro,
            'cidade' => $this->cidade,
            'estado' => $this->estado,
            'complemento' => $this->complemento,
            'ponto_referencia' => $this->ponto_referencia,
            'pais' => $this->pais,
        ]);

        $this->resetFields();

        session()->flash('success-livewire', 'Cliente criado com êxito.');
    }

    public function resetFields()
    {
        $this->nome = null;
        $this->email = null;
        $this->status = 0;
        $this->rg = null;
        $this->cpf = null;
        $this->cnpj = null;
        $this->site = null;
        $this->genero = null;
        $this->data_nascimento = null;
        $this->celular = null;
        $this->telefone = null;
        $this->obs = null;
        $this->cep = null;
        $this->rua = null;
        $this->numero = null;
        $this->bairro = null;
        $this->cidade = null;
        $this->estado = null;
        $this->complemento = null;
        $this->ponto_referencia = null;
        $this->pais = null;
    }

    public function setEndereco()
    {
        // Retorna endereço do ViaCEP.
        $response = Http::get("http://viacep.com.br/ws/{$this->cep}/json/");
        $viacep = $response->json();
        $this->rua    = $viacep['logradouro'] ?? null;
        $this->bairro = $viacep['bairro'] ?? null;
        $this->cidade = $viacep['localidade'] ?? null;
        $this->estado = $viacep['uf'] ?? null;
    }

    public function render()
    {
        return view('livewire.admin.cliente.add');
    }

}
