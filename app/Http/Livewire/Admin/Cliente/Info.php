<?php

namespace App\Http\Livewire\Admin\Cliente;

use Livewire\Component;
use App\Models\Cliente;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Info extends Component
{
    public $cliente;
    public $cadastrante;
    public $password_to_delete;

    public function mount($cliente_id)
    {
        $this->cliente = Cliente::find($cliente_id);

        $this->cadastrante = User::where('id', $this->cliente->cadastrante_id)->first();
    }

    public function render()
    {
        return view('livewire.admin.cliente.info');
    }

    public function delete()
    {
        if(\Auth::attempt(['id' => \Auth::id(), 'password' => $this->password_to_delete, 'status' => 0]))
        {
            Cliente::where('id', $this->cliente->id)->delete();

            session()->flash('success', 'Cliente deletado com êxito.');
            
            return redirect(route('admin.cliente.lista'));

        } else {

            session()->flash('danger-livewire', 'Senha incorreta.');

            $this->dispatchBrowserEvent('toast');
        }
    }
}
