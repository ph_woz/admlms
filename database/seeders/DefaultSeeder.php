<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class DefaultSeeder extends Seeder
{
    public function run()
    {
	    $user = User::create([
			'nome'     => 'Pedro Henrique Caetano da Silva',
			'email'    => 'pedrohenrique1207ph@gmail.com',
			'password' =>  Hash::make('26533955'),
	    ]);
	}

}