<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_clientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->string('nome');
            $table->string('email')->nullable();
            $table->integer('status')->default(0);
            $table->string('rg')->nullable();
            $table->string('cpf')->nullable();
            $table->string('cnpj')->nullable();
            $table->text('site')->nullable();
            $table->string('genero', 1)->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('celular')->nullable();
            $table->string('telefone')->nullable();
            $table->text('obs')->nullable();
            $table->string('cep')->nullable();
            $table->string('rua')->nullable();
            $table->string('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('complemento')->nullable();
            $table->string('ponto_referencia')->nullable();
            $table->string('pais')->nullable();
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_clientes');
    }
}
