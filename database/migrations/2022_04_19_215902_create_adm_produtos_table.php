<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_produtos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cadastrante_id')->nullable();
            $table->string('nome');
            $table->string('mensalista', 1)->default('N');
            $table->integer('status')->default(0);
            $table->text('descricao')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_produtos');
    }
}
