<div>

    <div class="col-12">
        @if (session()->has('success-livewire'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="alert-message">
                    Produto criado:
                    <span>
	                    <a href="{{ route('admin.produto.info', ['id' => $this->produto->id ?? null]) }}" class="weight-600">
	                    	{{ $produto->nome }} 
	                	</a>
	                </span>
                </div>
            </div>
        @endif
    </div>

    <form wire:submit.prevent="add" method="POST" class="mb-4">

		<section class="mb-4">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Mensalista</label>
			    		<select wire:model="mensalista" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>
			    	<div class="w-lg-45">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

				<div>
					<label>Descrição</label>
					<textarea wire:model="descricao" class="textarea-form" placeholder="Descrição"></textarea>
				</div>

			</div>

		</section>

		<button type="submit" class="btn-add">Cadastrar</button>	

    </form>

    @include('livewire.util.toast')
    
</div>