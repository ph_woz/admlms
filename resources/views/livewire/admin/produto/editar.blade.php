<div>

    <div class="mb-3 d-flex justify-content-end">
	    <a href="{{ route('admin.produto.info', ['id' => $this->produto->id ?? null]) }}" class="btn bg-base text-white">
	        <i class="fa fa-eye"></i>
	    </a>
	</div>

    <form wire:submit.prevent="editar" method="POST" class="mb-4">

		<section class="mb-4">
			
			<a href="#boxInfoPrincipal" data-bs-toggle="collapse" aria-expanded="false" class="hover-d-none">
				<div class="section-title">
					Informações Principais
				</div>
			</a>
			<hr/>
			
			<div class="collapse show" id="boxInfoPrincipal">

		    	<div class="d-lg-flex mb-3">

			    	<div class="w-100">
			    		<label>Nome</label>
			    		<input type="text" wire:model="nome" class="form-control p-form" placeholder="Nome" required>
			    	</div>
			    	<div class="w-100 mx-lg-3 my-3 my-lg-0">
			    		<label>Mensalista</label>
			    		<select wire:model="mensalista" class="select-form">
			    			<option value="N">Não</option>
			    			<option value="S">Sim</option>
			    		</select>
			    	</div>
			    	<div class="w-lg-45">
			    		<label>Status</label>
			    		<select wire:model="status" class="select-form">
			    			<option value="0">Ativo</option>
			    			<option value="1">Desativado</option>
			    		</select>
			    	</div>

			    </div>

				<div>
					<label>Descrição</label>
					<textarea wire:model="descricao" class="textarea-form" placeholder="Descrição"></textarea>
				</div>

			</div>

		</section>

		<button type="submit" class="btn-add">Salvar Alterações</button>	

    </form>

    @include('livewire.util.toast')
    
</div>