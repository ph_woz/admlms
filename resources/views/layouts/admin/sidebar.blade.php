<ul class="sidebar-nav">

    <li class="sidebar-item" id="url-dashboard">
        <a class="side-color sidebar-link" href="{{ route('admin.dashboard') }}">
            <i class="side-color fa fa-home"></i> <span class="align-middle"></span> Dashboard
        </a>
    </li>
    <li class="sidebar-item" id="url-fluxo-caixa">
        <a class="side-color sidebar-link" href="{{ route('admin.controle-financeiro.lista.fluxo-caixa') }}">
            <i class="side-color fas fa-hand-holding-usd"></i> <span class="align-middle"></span> Fluxo de Caixa
        </a>
    </li>
    <li class="sidebar-item" id="url-chamado">
        <a class="side-color sidebar-link" href="{{ route('admin.chamado.lista') }}">
            <i class="side-color fa fa-home"></i> <span class="align-middle"></span> Chamados
        </a>
    </li>
    <li class="sidebar-item" id="url-cliente">
        <a class="side-color sidebar-link" href="{{ route('admin.cliente.lista') }}">
            <i class="side-color fa fa-users"></i> <span class="align-middle"></span> Clientes
        </a>
    </li>
    <li class="sidebar-item" id="url-produto">
        <a class="side-color sidebar-link" href="{{ route('admin.produto.lista') }}">
            <i class="side-color fa fa-box-open"></i> <span class="align-middle"></span> Produtos
        </a>
    </li>
</ul>
