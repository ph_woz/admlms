<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-start">
                <p class="mb-0">
                </p>
            </div>
            <div class="col-6 text-end">
                <ul class="list-inline">
                    <li class="list-inline-item text-muted">
                        &copy; ADM LMS WOZCODE
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
