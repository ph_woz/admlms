<li class="nav-item dropdown">

    <a class="nav-link dropdown-toggle border-0 text-dark" href="#" data-bs-toggle="dropdown">
    	<i class="fas fa-user-circle fs-17"></i>
        <span>
            {{ strtok(Auth::user()->nome, " ") }}
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-end">
        <a class="dropdown-item" href="{{ route('logout') }}"><i class="align-middle" data-feather="log-out"></i> Sair</a>
    </div>
</li>
