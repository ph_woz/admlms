<!doctype html>
<html lang="pt-br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="shortcut icon" href="https://wozcode.com/images/favicon.ico">

    <title>ADM - LMS WOZCODE</title>

</head>
<body class="bg-danger">

    <header>
        <div class="bg-white text-center d-flex align-items-center justify-content-center" style="height: 80px;">
            <a href="{{ route('login') }}">
                <img src="https://wozcode.com/images/logo.png" class="img-fluid" style="height: 50px;">
            </a>
        </div>
    </header>

    <main class="container-fluid px-xl-5 pt-5">

        <section class="container">

            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    <b>Sucesso!</b>
                    {{ session('success') }}
                </div>
            @endif

            @if(session('danger'))
                <div class="alert alert-danger" role="alert">
                    <b>Ops!</b>
                    {{ session('danger') }}
                </div>
            @endif


            <form method="POST" class="p-5 bg-white shadow p-3 rounded mt-4">
            @csrf

                <div class="mb-5">

                    <p class="mb-0">Informações de Usuário</p>
                    <hr class="mt-2" />
                    <div class="d-flex">
                        <input type="text" name="nome" class="form-control" placeholder="Nome" value="Pedro Henrique" required>
                        <input type="text" name="email" class="mx-lg-3 my-3 my-lg-0 form-control" placeholder="Email" value="pedrohenrique1207ph@gmail.com" required>
                        <input type="password" name="password" class="form-control" placeholder="Senha" required>
                    </div>

                </div>
                <div>
                    
                    <p class="mb-0">Informações da Plataforma</p>
                    <hr class="mt-2" />
                    <div class="d-flex">
                        <input type="text" name="nome_plataforma" class="form-control" placeholder="Nome da Instituição" required>
                        <input type="text" name="dominio" class="mx-lg-3 my-3 my-lg-0 form-control" placeholder="example.com" required>
                        <select name="plataforma_id" class="form-control" required>
                            <option value="">Selecione a Plataforma a ser copiada</option>
                            @foreach($plataformas as $plataforma)
                                <option value="{{ $plataforma->id }}">
                                    {{ $plataforma->nome }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <button type="submit" class="btn btn-primary mt-5" name="sent" value="ok">
                    Clonar!
                </button>

            </form>

        </section>

    </main>

</body>
</html>