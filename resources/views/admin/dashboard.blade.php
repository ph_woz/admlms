@extends('layouts.admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="mb-3">
                <a href="/admin/clonar" target="_blank" class="btn btn-success weight-600">
                    Gerar nova Plataforma
                </a>
            </div>

            <form class="d-flex mb-4">
                <select name="status[]" class="selectpicker rounded-right-0" data-width="100%" multiple title="Ativo">
                    <option value="0" @if(in_array(0, $status)) selected @endif>Ativos</option>
                    <option value="1" @if(in_array(1, $status)) selected @endif>Inadimplentes</option>
                    <option value="2" @if(in_array(2, $status)) selected @endif>Cancelados</option>
                    <option value="3" @if(in_array(3, $status)) selected @endif>Trial</option>
                </select>
                <button type="submit" name="sent" value="ok" class="btn btn-primary rounded-left-0 px-3">
                    <i class="fa fa-search"></i>
                </button>
            </form>

            <div class="bg-white pt-2 border mb-5">
                <span class="px-3 pb-2 d-block">Plataformas ({{count($plataformas)}})</span>
                <ul class="list-group list-group-flush">
                    @foreach($plataformas as $plataforma)
                    <a href="{{ route('admin.plataforma.info', ['id' => $plataforma->id]) }}" class="border-bottom hover-d-none text-dark bg-hover-gray font-nunito weight-600">
                        <li class="d-flex justify-content-between align-items-center list-group-item bg-hover-f9">
                            <span>
                                {{ $plataforma->nome }}  ({{ $plataforma->dominio }})
                            </span>
                            <span>
                                @if($plataforma->status == 0)
                                    <span class="bg-success px-2 py-1 fs-12 text-white rounded text-shadow-1">Ativo</span>
                                @endif
                                @if($plataforma->status == 1)
                                    <span class="bg-danger px-2 py-1 fs-12 text-white rounded text-shadow-1">Inadimplente</span>
                                @endif
                                @if($plataforma->status == 2)
                                    <span class="bg-danger px-2 py-1 fs-12 text-white rounded text-shadow-1">Cancelado</span>
                                @endif
                                @if($plataforma->status == 3)
                                    <span class="bg-secondary px-2 py-1 fs-12 text-white rounded text-shadow-1">Trial</span>
                                @endif
                            </span>
                        </li>
                    </a>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>  
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
@endpush