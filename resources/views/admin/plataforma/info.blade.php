@extends('layouts.admin.master')
@section('content')

<div>

	<section>

		<div class="section-title rounded-bottom-0 d-flex justify-content-between align-items-center status-{{$plataforma->status}}">
			{{ $plataforma->nome }}
			<div>
				<a href="#" data-bs-toggle="modal" data-bs-target="#ModalChangeStatus" class="d-block hover-d-none py-2" onclick="document.getElementById('status').value = '{{ $plataforma->status }}';">
	                <span>
	                    @if($plataforma->status == 0)
	                        <span class="bg-success px-3 py-2 weight-600 fs-12 text-white rounded text-shadow-2">Ativo</span>
	                    @endif
	                    @if($plataforma->status == 1)
	                        <span class="bg-danger px-3 py-2 weight-600 fs-12 text-white rounded text-shadow-2">Inadimplente</span>
	                    @endif
	                    @if($plataforma->status == 2)
	                        <span class="bg-danger px-3 py-2 weight-600 fs-12 text-white rounded text-shadow-2">Cancelado</span>
	                    @endif
	                    @if($plataforma->status == 3)
	                        <span class="bg-secondary px-3 py-2 weight-600 fs-12 text-white rounded text-shadow-2">Trial</span>
	                    @endif
	                </span>
				</a>
			</div>
		</div>

		<div class="bg-white border p-4 d-lg-flex justify-content-between align-items-center">

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Quantidade de alunos ativos</p>
				<p class="mb-0">
					{{ $countAlunosAtivos }}
				</p>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Domínio</p>
				<p class="mb-0">
					<a href="https://{{ $plataforma->dominio }}" target="_blank" class="color-blue-info-link">
						{{ $plataforma->dominio }}
					</a>
				</p>
			</div>

			<div class="mb-2 mb-lg-0">
				<p class="mb-0 weight-600 color-444">Responsável</p>
				<p class="mb-0">
					@if(isset($responsavel))
						{{ $responsavel->nome }}
						<br>
						{{ $responsavel->email }}
					@else
						Não definido
					@endif
				</p>
			</div>

		</div>

	</section>

	<form method="POST">
	@csrf
		<div class="modal fade" id="ModalChangeStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  	<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<h3 class="modal-title">Mudar Status da Plataforma</h3>
		    			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      		</div>
		      		<div class="modal-body">

		      			<select name="status" id="status" class="select-form">
		      				<option value="0">Ativo</option>
		      				<option value="1">Inadimplente</option>
		      				<option value="2">Cancelado</option>
		      				<option value="3">Trial</option>
		      			</select>

		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" name="sent" value="ok" class="btn btn-primary weight-600">Confirmar</button>
		      		</div>
		    	</div>
		  	</div>
		</div>
	</form>

@endsection